package com.spectral369.utils;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableScheduling
public class Scheduler {

	@Scheduled(cron = "0 0 */3 * * ?")
	public void runTask() {
		PDFHelper.setCheckInt(Utils.checkIfOK(Utils.getid()));
	}

}
