package com.spectral369.utils;

import java.io.IOException;

import com.itextpdf.io.font.constants.StandardFonts;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfPage;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.kernel.pdf.event.AbstractPdfDocumentEvent;
import com.itextpdf.kernel.pdf.event.AbstractPdfDocumentEventHandler;
import com.itextpdf.kernel.pdf.event.PdfDocumentEvent;

public class FooterEvt extends AbstractPdfDocumentEventHandler {
	float width = 0;

	@Override
	public void onAcceptedEvent(AbstractPdfDocumentEvent event) {
		PdfDocumentEvent docEvent = (PdfDocumentEvent) event;
		PdfPage page = docEvent.getPage();
		int pageNum = docEvent.getDocument().getPageNumber(page);
		int pageTotal = docEvent.getDocument().getNumberOfPages();
		PdfCanvas canvas = new PdfCanvas(page);
		canvas.beginText();
		try {
			canvas.setFontAndSize(PdfFontFactory.createFont(StandardFonts.HELVETICA), 12);
		} catch (IOException e) {
			e.printStackTrace();
		}

		canvas.moveText(width / 2, 0);
		canvas.showText(String.format("%d/%d", pageNum, pageTotal));
		canvas.endText();
		canvas.stroke();
		canvas.release();

	}

	public FooterEvt(float width) {
		this.width = width;
	}

}