package com.spectral369.DPD;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.event.PdfDocumentEvent;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Tab;
import com.itextpdf.layout.element.TabStop;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.element.Text;
import com.itextpdf.layout.properties.HorizontalAlignment;
import com.itextpdf.layout.properties.TabAlignment;
import com.itextpdf.layout.properties.TextAlignment;
import com.spectral369.birotica.PdfList;
import com.spectral369.utils.FooterEvt;
import com.spectral369.utils.PDFHelper;
import com.spectral369.utils.Utils;

public class DeclaratiePrelucrareDateCreator {
	File pdff = null;
	private transient PdfDocument document;
	private transient PdfWriter writer;
	String id = Utils.getRandomStringSerial("DPD");

	public DeclaratiePrelucrareDateCreator(Map<String, String> map, String tm) {

		pdff = new File(Utils.getSaveFileLocation("Declaratie_Prelucrare_date_" + tm + ".pdf"));
		if (map.isEmpty()) {
			generatePDF(tm, pdff);
			PdfList.addFile(id, pdff.getAbsolutePath());

		} else {

			generatePDF(tm, pdff, map);
			PdfList.addFile(id, pdff.getAbsolutePath());
		}
	}

	public String getID() {
		return id;
	}

	private void generatePDF(final String tm, final File pdfFile) {
		try {
			writer = new PdfWriter(pdfFile);
			Image antetLogo = PDFHelper.getAntetLogo();
			document = new PdfDocument(writer);
			document.getDocumentInfo().addCreationDate();
			document.getDocumentInfo().setAuthor("spectral369");
			document.getDocumentInfo().setTitle("Declaratie_Prelucrare_Date_" + tm);
			document.setDefaultPageSize(PageSize.A4);
			Document doc = new Document(document);
			// doc.setFontSize(11.0f);
			float width = doc.getPageEffectiveArea(PageSize.A4).getWidth();
			float marginLeft = doc.getLeftMargin();
			float marginRIght = doc.getRightMargin();
			document.addEventHandler(PdfDocumentEvent.END_PAGE, new FooterEvt(width + (marginLeft + marginRIght)));

			final Paragraph antet = new Paragraph();

			float documentWidth = document.getDefaultPageSize().getWidth() - doc.getLeftMargin() - doc.getRightMargin();

			float documentHeight = document.getDefaultPageSize().getHeight() - doc.getTopMargin()
					- doc.getBottomMargin();

			antetLogo.scaleToFit(documentWidth - 50, documentHeight - 80);

			antet.add(antetLogo);
			antet.setHorizontalAlignment(HorizontalAlignment.CENTER);
			antet.setTextAlignment(TextAlignment.CENTER);
			doc.add(antet);

			/*
			 * final Paragraph nrInreg = new Paragraph(); nrInreg.add("\n\n");
			 * nrInreg.add(new Tab()); Text nrI = new Text( "\tNr. " +
			 * PDFHelper.getStrWithDash(12, "") + " " + "/ " + PDFHelper.getStrWithDash(18,
			 * "")); nrInreg.add(nrI); doc.add(nrInreg);
			 */

			final Paragraph titlu = new Paragraph();
			Text t1 = new Text("\n\nDeclaratie privind prelucrarea datelor cu caracter personal\n\n")
					/* .setCharacterSpacing(2f) */.simulateBold().setFontSize(16f);
			titlu.setHorizontalAlignment(HorizontalAlignment.CENTER);
			titlu.setTextAlignment(TextAlignment.CENTER);
			titlu.add(t1).addStyle(PDFHelper.bold12nr);
			doc.add(titlu);

			final Paragraph declaratie = new Paragraph();
			declaratie.add(PDFHelper.addTab());
			/*
			 * Text dec1 = new Text("Se adevereste prin prezenta ca dl./d-na. " +
			 * PDFHelper.getStrWithDash(32, "") + " domiciliat/a in comuna " +
			 * PDFHelper.getStrWithDash(20, "") + ", nr. " + PDFHelper.getStrWithDash(6, "")
			 * + ", avand C.N.P " + PDFHelper.getStrWithDash(18, "") + "," +
			 * " figureaza in registrul agricol al localitatii " +
			 * PDFHelper.getStrWithDash(25, "") + " cu urmatoarea stare materiala:\n");
			 */
			Text dec1 = new Text("Subsemnatul/Subsemnata " + PDFHelper.getStrWithDash(46, "") + " avand CNP "
					+ PDFHelper.getStrWithDash(25, "") + " nascut(a) la data de " + PDFHelper.getStrWithDash(12, "")
					+ " in localitatea " + PDFHelper.getStrWithDash(18, "") + ", domiciliat(a) in "
					+ PDFHelper.getStrWithDash(26, "") + " str " + PDFHelper.getStrWithDash(18, "") + " nr. "
					+ PDFHelper.getStrWithDash(6, "") + " bloc " + PDFHelper.getStrWithDash(4, "") + " scara "
					+ PDFHelper.getStrWithDash(4, "") + " ap. " + PDFHelper.getStrWithDash(4, "") + " jud. "
					+ PDFHelper.getStrWithDash(12, "") + " posesor al C.I. seria " + PDFHelper.getStrWithDash(8, "")
					+ "\n nr. " + PDFHelper.getStrWithDash(9, "") + " eliberata de " + PDFHelper.getStrWithDash(22, "")
					+ " la data de " + PDFHelper.getStrWithDash(12, "")
					+ ", prin prezenta imi exprim acordul cu privire la utilizarea si prelucrarea "
					+ "datelor cu caracter personal, conform Regulamentul(UE) 2016/679 al Parlamentului European si al Consiliului din 27 Aprilie 2016 "
					+ "privind protectia persoanelor fizice in ceea ce priveste prelucrarea datelor cu caracter personal si privind libera circulatie a acestor date "
					+ "catre Comuna Dudestii-Vechi, jud. Timis.\n\n ");
			declaratie.add(dec1);
			declaratie.add(PDFHelper.addTab());
			Text dec2 = new Text(
					"Totodata sunt de acord ca toate documentele emise de autoritate sa-mi fie comunicate la adresa de "
							+ "e-mail " + PDFHelper.getStrWithDash(28, "") + ", telefon "
							+ PDFHelper.getStrWithDash(22, "") + ", adresa de corespondenta: \n");
			declaratie.add(dec2);
			Text aceiasi = new Text(" [  ] aceeasi ca si adresa de mai sus.\n");
			Text alta = new Text(" [  ] alta: " + PDFHelper.getStrWithDash(70, ""));

			declaratie.add(aceiasi);
			declaratie.add(alta);
			declaratie.add("\n\n\n\n");
			doc.add(declaratie);

			Table semnaturi = new Table(1);

			semnaturi.setHorizontalAlignment(HorizontalAlignment.CENTER);
			Paragraph p1 = new Paragraph();
			Text primar = new Text("Data");
			p1.add(primar);
			p1.addTabStops(new TabStop(width / 1.18f, TabAlignment.RIGHT));
			p1.add(new Tab());
			Text intocmit = new Text("Semnatura");
			p1.add(intocmit);
			Cell cell1 = new Cell();
			cell1.setBorder(Border.NO_BORDER);
			cell1.add(p1);
			semnaturi.addCell(cell1);
			doc.add(semnaturi);

			Table semnaturiR = new Table(1);

			semnaturiR.setHorizontalAlignment(HorizontalAlignment.CENTER);
			Paragraph semnR = new Paragraph();
			// String dateNow = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
			Text semnPrim = new Text(PDFHelper.getStrWithDash(22, ""));
			semnR.add(semnPrim);
			semnR.addTabStops(new TabStop(width / 1.18f, TabAlignment.RIGHT));
			semnR.add(new Tab());
			Text semnIntocmit = new Text(PDFHelper.getStrWithDash(22, ""));
			semnR.add(semnIntocmit);
			Cell cell2 = new Cell();
			cell2.setBorder(Border.NO_BORDER);
			cell2.add(semnR);
			semnaturiR.addCell(cell2);
			doc.add(semnaturiR);

			Paragraph footer = new Paragraph();
			footer.add("\n\n\n\n");
			footer.setTextAlignment(TextAlignment.RIGHT);
			Text footer01 = new Text("Document care contine date cu caracter personal\n protejate de"
					+ " prevederile Regulamentului (UE) 2016/679\n");
			Text footer02 = new Text("WWW.ANAF.RO").simulateBold();
			footer.add(footer01);
			footer.add(footer02);
			doc.add(footer);

			doc.close();
			document.close();
			writer.flush();

		} catch (IOException ex2) {
		}

	}

	private void generatePDF(final String tm, final File pdfFile, final Map<String, String> map) {
		try {
			writer = new PdfWriter(pdfFile);
			Image antetLogo = PDFHelper.getAntetLogo();
			document = new PdfDocument(writer);
			document.getDocumentInfo().addCreationDate();
			document.getDocumentInfo().setAuthor("spectral369");
			document.getDocumentInfo().setTitle("Declaratie_Prelucrare_Date_" + tm);
			document.setDefaultPageSize(PageSize.A4);
			Document doc = new Document(document);
			// doc.setFontSize(11.0f);
			float width = doc.getPageEffectiveArea(PageSize.A4).getWidth();
			float marginLeft = doc.getLeftMargin();
			float marginRIght = doc.getRightMargin();
			document.addEventHandler(PdfDocumentEvent.END_PAGE, new FooterEvt(width + (marginLeft + marginRIght)));

			final Paragraph antet = new Paragraph();

			float documentWidth = document.getDefaultPageSize().getWidth() - doc.getLeftMargin() - doc.getRightMargin();

			float documentHeight = document.getDefaultPageSize().getHeight() - doc.getTopMargin()
					- doc.getBottomMargin();

			antetLogo.scaleToFit(documentWidth - 50, documentHeight - 80);

			antet.add(antetLogo);
			antet.setHorizontalAlignment(HorizontalAlignment.CENTER);
			antet.setTextAlignment(TextAlignment.CENTER);
			doc.add(antet);

			/*
			 * final Paragraph nrInreg = new Paragraph(); nrInreg.add("\n\n");
			 * nrInreg.add(new Tab()); Text nrI = new Text( "\tNr. " +
			 * PDFHelper.getStrWithDash(12, "") + " " + "/ " + PDFHelper.getStrWithDash(18,
			 * "")); nrInreg.add(nrI); doc.add(nrInreg);
			 */

			final Paragraph titlu = new Paragraph();
			Text t1 = new Text("\n\nDeclaratie privind prelucrarea datelor cu caracter personal\n\n")
					/* .setCharacterSpacing(2f) */.simulateBold().setFontSize(16f);
			titlu.setHorizontalAlignment(HorizontalAlignment.CENTER);
			titlu.setTextAlignment(TextAlignment.CENTER);
			titlu.add(t1).addStyle(PDFHelper.bold12nr);
			doc.add(titlu);

			String num = null;
			String dom = null;
			String nasc = null;
			if (map.get("titlu").contains("Dl")) {
				num = new String("Subsemnatul");
				dom = new String("domiciliat");
				nasc = new String("nascut");
			} else {
				num = new String("Subsemnata");
				dom = new String("domiciliata");
				nasc = new String("nascuta");
			}

			final Paragraph declaratie = new Paragraph();

			declaratie.add(new Tab());
			declaratie.add(num + " ");
			declaratie.add(PDFHelper.createAdjustableParagraph(52,
					new Paragraph(map.get("nume")).simulateBold().setTextAlignment(TextAlignment.CENTER)));
			declaratie.add(" avand CNP ");
			declaratie.add(PDFHelper.createAdjustableParagraph(30,
					new Paragraph(map.get("cnp")).simulateBold().setTextAlignment(TextAlignment.CENTER)));
			declaratie.add(" " + nasc + " in data de ");
			declaratie.add(PDFHelper.createAdjustableParagraph(18,
					new Paragraph(map.get("dataNastere")).simulateBold().setTextAlignment(TextAlignment.CENTER)));
			declaratie.add(" in localitatea ");
			declaratie.add(PDFHelper.createAdjustableParagraph(40,
					new Paragraph(map.get("localitateNastere")).simulateBold().setTextAlignment(TextAlignment.CENTER)));
			declaratie.add(" " + dom + " in ");
			declaratie.add(PDFHelper.createAdjustableParagraph(40,
					new Paragraph(map.get("domiciliu")).simulateBold().setTextAlignment(TextAlignment.CENTER)));
			declaratie.add(" str. ");
			declaratie.add(PDFHelper.createAdjustableParagraph(34,
					new Paragraph(map.get("strada")).simulateBold().setTextAlignment(TextAlignment.CENTER)));
			declaratie.add(" nr. ");
			declaratie.add(PDFHelper.createAdjustableParagraph(14,
					new Paragraph(map.get("nrCasaAddr")).simulateBold().setTextAlignment(TextAlignment.CENTER)));
			declaratie.add(" bloc ");
			declaratie.add(PDFHelper.createAdjustableParagraph(4,
					new Paragraph(map.get("bloc")).simulateBold().setTextAlignment(TextAlignment.CENTER)));
			declaratie.add(" scara ");
			declaratie.add(PDFHelper.createAdjustableParagraph(4,
					new Paragraph(map.get("scara")).simulateBold().setTextAlignment(TextAlignment.CENTER)));
			declaratie.add(" ap. ");
			declaratie.add(PDFHelper.createAdjustableParagraph(4,
					new Paragraph(map.get("apartament")).simulateBold().setTextAlignment(TextAlignment.CENTER)));
			declaratie.add(" jud. ");
			declaratie.add(PDFHelper.createAdjustableParagraph(22,
					new Paragraph(map.get("judet")).simulateBold().setTextAlignment(TextAlignment.CENTER)));
			declaratie.add(" posesor al C.I. seria ");
			declaratie.add(PDFHelper.createAdjustableParagraph(8, new Paragraph(map.get("serie").toUpperCase())
					.simulateBold().setTextAlignment(TextAlignment.CENTER)));
			declaratie.add(" nr. ");
			declaratie.add(PDFHelper.createAdjustableParagraph(13,
					new Paragraph(map.get("nrci")).simulateBold().setTextAlignment(TextAlignment.CENTER)));
			declaratie.add(" eliberata de ");
			declaratie.add(PDFHelper.createAdjustableParagraph(37,
					new Paragraph(map.get("cieliberat")).simulateBold().setTextAlignment(TextAlignment.CENTER)));
			declaratie.add(" la data de ");
			declaratie.add(PDFHelper.createAdjustableParagraph(18,
					new Paragraph(map.get("dataci")).simulateBold().setTextAlignment(TextAlignment.CENTER)));
			declaratie.add(", prin prezenta imi exprim acordul cu  cu privire la utilizarea si prelucrarea "
					+ " datelor cu caracter personal, conform Regulamentul(UE) 2016/679 al Parlamentului European si al Consiliului din 27 Aprilie 2016"
					+ "	privind protectia persoanelor fizice in ceea ce priveste prelucrarea datelor cu caracter personal si privind libera circulatie a acestor date "
					+ " catre Comuna Dudestii-Vechi, jud. Timis.\n\n");
			declaratie.add(PDFHelper.addTab());
			declaratie.add("Totodata sunt de acord ca toate documentele emise de autoritatesa-mi fie comunicate la "
					+ "adresa de email ");
			declaratie.add(PDFHelper.createAdjustableParagraph(42,
					new Paragraph(map.get("email")).simulateBold().setTextAlignment(TextAlignment.CENTER)));
			declaratie.add(", telefon ");
			declaratie.add(PDFHelper.createAdjustableParagraph(24,
					new Paragraph(map.get("telefon")).simulateBold().setTextAlignment(TextAlignment.CENTER)));
			declaratie.add(", adresa de corespondenta: \n");
			if (map.get("adresaAlternativa") == null || map.get("adresaAlternativa").isEmpty()) {
				declaratie.add(" [ x ] aceeasi ca si adresa de mai sus.\n");
				declaratie.add(" [  ] alta: ");
				declaratie.add(PDFHelper.getStrWithDash(70, ""));
			} else {
				declaratie.add(" [  ] aceeasi ca si adresa de mai sus.\n");
				declaratie.add(" [ x ] alta: ");
				declaratie.add(PDFHelper.createAdjustableParagraph(68, new Paragraph(map.get("adresaAlternativa"))
						.simulateBold().setTextAlignment(TextAlignment.CENTER)));
			}

			declaratie.add("\n\n\n\n");
			doc.add(declaratie);

			Table semnaturi = new Table(1);

			semnaturi.setHorizontalAlignment(HorizontalAlignment.CENTER);
			Paragraph p1 = new Paragraph();
			Text primar = new Text("Data");
			p1.add(primar);
			p1.addTabStops(new TabStop(width / 1.18f, TabAlignment.RIGHT));
			p1.add(new Tab());
			Text intocmit = new Text("Semnatura");
			p1.add(intocmit);
			Cell cell1 = new Cell();
			cell1.setBorder(Border.NO_BORDER);
			cell1.add(p1);
			semnaturi.addCell(cell1);
			doc.add(semnaturi);

			Table semnaturiR = new Table(1);

			semnaturiR.setHorizontalAlignment(HorizontalAlignment.CENTER);
			Paragraph semnR = new Paragraph();
			// String dateNow = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
			Text semnPrim = new Text(PDFHelper.getStrWithDash(22, ""));
			semnR.add(semnPrim);
			semnR.addTabStops(new TabStop(width / 1.18f, TabAlignment.RIGHT));
			semnR.add(new Tab());
			Text semnIntocmit = new Text(PDFHelper.getStrWithDash(22, ""));
			semnR.add(semnIntocmit);
			Cell cell2 = new Cell();
			cell2.setBorder(Border.NO_BORDER);
			cell2.add(semnR);
			semnaturiR.addCell(cell2);
			doc.add(semnaturiR);

			Paragraph footer = new Paragraph();
			footer.add("\n\n\n\n");
			footer.setTextAlignment(TextAlignment.RIGHT);
			Text footer01 = new Text("Document care contine date cu caracter personal\n protejate de"
					+ " prevederile Regulamentului (UE) 2016/679\n");
			Text footer02 = new Text("WWW.ANAF.RO").simulateBold();
			footer.add(footer01);
			footer.add(footer02);
			doc.add(footer);

			doc.close();
			document.close();
			writer.flush();

		} catch (IOException ex2) {
		}

	}

}
