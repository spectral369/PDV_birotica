package com.spectral369.DPD;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import com.spectral369.birotica.MainView;
import com.spectral369.utils.PDFHelper;
import com.spectral369.utils.Utils;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.Setter;
import com.vaadin.flow.data.converter.StringToBigIntegerConverter;
import com.vaadin.flow.data.converter.StringToIntegerConverter;
import com.vaadin.flow.function.ValueProvider;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;
import com.vaadin.flow.router.QueryParameters;
import com.vaadin.flow.router.RouteConfiguration;
import com.vaadin.flow.router.RouterLayout;

public class DeclaratiePrelucrareDateInfo extends HorizontalLayout implements RouterLayout, AfterNavigationObserver {
	private static final long serialVersionUID = 1L;
	public static final String NAME = "DeclaratiePrelucrareDateInfo";
	VerticalLayout content;
	HorizontalLayout backLayout;
	Button backbtn;
	HorizontalLayout titlelLayout;
	Button IarbaTitle;
	HorizontalLayout checkLayout;
	Checkbox complete;
	HorizontalLayout infoLayout;
	VerticalLayout infoPart1;
	VerticalLayout infoPart2;
	VerticalLayout infoPart3;
	VerticalLayout infoPart310;
	VerticalLayout infoPart31;
	VerticalLayout infoPart4;
	VerticalLayout infoPart11;
	VerticalLayout infoPart5;
	HorizontalLayout infoLayout1;
	HorizontalLayout infoLayout2;
	HorizontalLayout infoLayout3;
	HorizontalLayout infoLayout4;
	VerticalLayout infoPart6;
	VerticalLayout infoPart7;
	VerticalLayout infopart8;
	VerticalLayout infoPart9;
	VerticalLayout infoPart10;
	VerticalLayout infoPart12;
	VerticalLayout infoPart13;
	VerticalLayout infoPart14;
	VerticalLayout infoPart15;
	VerticalLayout infoPart16;

	TextField prenumeField;
	TextField numeField;
	DatePicker dataNasteriiField;
	TextField localitate;
	TextField cnpField;
	TextField nrStrField;
	ComboBox<String> detinePamant;
	ComboBox<String> titlu;
	ComboBox<String> intocmit;

	HorizontalLayout generateLayout;
	Button generate;
	DeclaratiePrelucrareDatePDF pdf;
	Map<String, String> map;
	private Binder<DeclaratiePrelucrareDateInfo> binder;
	private VerticalLayout infoPart311;
	private TextField localitateNastere;
	private TextField blocField;
	private TextField scaraField;
	private TextField apartamentField;
	private TextField judetField;
	private TextField serieField;
	private TextField nrSerieField;
	private VerticalLayout infoPart41;
	private TextField stradaField;
	private TextField eliberatDeField;
	private DatePicker dataEliberareField;
	private EmailField emailField;
	private VerticalLayout infoPart17;
	private TextField nrTelefon;
	private VerticalLayout infoPart18;
	private ComboBox<String> AdresaCorespondenta;
	private VerticalLayout infoPart19;
	private TextField AdresaCorespondentaAlternativa;

	public DeclaratiePrelucrareDateInfo() {
		content = new VerticalLayout();
		map = new HashMap<String, String>();
		binder = new Binder<DeclaratiePrelucrareDateInfo>(DeclaratiePrelucrareDateInfo.class);
		backLayout = new HorizontalLayout();
		backLayout.setMargin(true);
		backbtn = new Button("Back", VaadinIcon.ARROW_CIRCLE_LEFT.create());
		backbtn.getClassNames().add("quiet");
		backbtn.addClickListener(evt -> {
			RouteConfiguration.forSessionScope().removeRoute(NAME);
			UI.getCurrent().navigate(MainView.class);
		});
		backLayout.add(backbtn);
		content.add(backLayout);
		content.setAlignItems(Alignment.START);
		titlelLayout = new HorizontalLayout();
		IarbaTitle = new Button("Declaratie Prelucrare Date", VaadinIcon.FILE_TEXT_O.create());
		IarbaTitle.setEnabled(false);
		IarbaTitle.setId("mainTitle");
		IarbaTitle.addClassName("clearDisabled");
		titlelLayout.add(IarbaTitle);
		content.add(titlelLayout);
		content.setAlignItems(Alignment.CENTER);
		checkLayout = new HorizontalLayout();
		(complete = new Checkbox("Cu completare de date ?!", false)).addValueChangeListener(evt -> {
			toggleVisibility();
		});
		checkLayout.add(complete);
		content.add(checkLayout);
		content.setAlignItems(Alignment.CENTER);
		(infoLayout = new HorizontalLayout()).setVisible(false);

		infoPart1 = new VerticalLayout();
		titlu = new ComboBox<String>("Titlul: ");
		titlu.setItems("Dl.", "D-na.");
		titlu.setRequired(true);
		titlu.setValue("Dl.");
		titlu.setRequiredIndicatorVisible(true);
		binder.forField(titlu).asRequired().bind(new ValueProvider<DeclaratiePrelucrareDateInfo, String>() {

			private static final long serialVersionUID = 1L;

			@Override
			public String apply(DeclaratiePrelucrareDateInfo source) {
				return null;
			}
		}, new Setter<DeclaratiePrelucrareDateInfo, String>() {

			private static final long serialVersionUID = 1L;

			@Override
			public void accept(DeclaratiePrelucrareDateInfo bean, String fieldvalue) {

			}
		});

		titlu.setErrorMessage("Titlu Required !");
		infoPart1.add(titlu);
		infoLayout.add(infoPart1);
		infoLayout.setAlignItems(Alignment.CENTER);

		infoPart2 = new VerticalLayout();
		numeField = new TextField("Nume Complet:");
		numeField.setRequiredIndicatorVisible(true);

		binder.forField(numeField).asRequired()
				.withValidator(str -> str.length() > 5, "Numele sa fie mai mare decat 5 caractere")
				.bind(new ValueProvider<DeclaratiePrelucrareDateInfo, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public String apply(DeclaratiePrelucrareDateInfo source) {
						return null;
					}
				}, new Setter<DeclaratiePrelucrareDateInfo, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public void accept(DeclaratiePrelucrareDateInfo bean, String fieldvalue) {

					}
				});

		numeField.setErrorMessage("Nume Required !");
		numeField.setWidth("100%");
		numeField.setMaxLength(25);
		numeField.setMinWidth(20f, Unit.EM);
		infoPart2.add(numeField);
		infoLayout.add(infoPart2);
		infoLayout.setAlignItems(Alignment.CENTER);

		infoPart3 = new VerticalLayout();
		cnpField = new TextField("CNP:");
		cnpField.setRequiredIndicatorVisible(true);
		cnpField.addValueChangeListener(e -> {
			cnpField.setValue(cnpField.getValue().replaceAll("[a-zA-Z]+", ""));
		});

		binder.forField(cnpField).asRequired()
				.withValidator(str -> str.toString().length() == 13, "CNP-ul are 13 cifre")
				.withConverter(new StringToBigIntegerConverter("CNP-ul poate contine doar cifre"))

				.bind(new ValueProvider<DeclaratiePrelucrareDateInfo, BigInteger>() {

					private static final long serialVersionUID = 1L;

					@Override
					public BigInteger apply(DeclaratiePrelucrareDateInfo source) {
						return null;
					}
				}, new Setter<DeclaratiePrelucrareDateInfo, BigInteger>() {

					private static final long serialVersionUID = 1L;

					@Override
					public void accept(DeclaratiePrelucrareDateInfo bean, BigInteger fieldvalue) {

					}
				});

		cnpField.setErrorMessage("CNP Required !");
		infoPart3.add(cnpField);
		infoLayout.add(infoPart3);
		infoLayout.setAlignItems(Alignment.CENTER);

		infoPart310 = new VerticalLayout();
		DatePicker.DatePickerI18n singleFormatI18n = new DatePicker.DatePickerI18n();
		singleFormatI18n.setDateFormat("dd-MM-yyyy");

		dataNasteriiField = new DatePicker("Data nasterii:");
		dataNasteriiField.setI18n(singleFormatI18n);
		dataNasteriiField.setRequiredIndicatorVisible(true);

		binder.forField(dataNasteriiField).asRequired()

				.bind(new ValueProvider<DeclaratiePrelucrareDateInfo, LocalDate>() {

					private static final long serialVersionUID = 1L;

					@Override
					public LocalDate apply(DeclaratiePrelucrareDateInfo source) {
						return null;
					}
				}, new Setter<DeclaratiePrelucrareDateInfo, LocalDate>() {

					private static final long serialVersionUID = 1L;

					@Override
					public void accept(DeclaratiePrelucrareDateInfo bean, LocalDate fieldvalue) {

					}
				});

		dataNasteriiField.setErrorMessage("Data Nasterii Required !");
		infoPart310.add(dataNasteriiField);
		infoLayout.add(infoPart310);
		infoLayout.setAlignItems(Alignment.CENTER);

		infoLayout1 = new HorizontalLayout();
		infoLayout1.setVisible(false);

		infoPart311 = new VerticalLayout();
		localitateNastere = new TextField("Localitatea nasterii: ");
		localitateNastere.setRequiredIndicatorVisible(true);

		binder.forField(localitateNastere).asRequired()
				.withValidator(str -> str.toString().length() > 3, "Localitatea necesita cel putin 3 caractere")
				.bind(new ValueProvider<DeclaratiePrelucrareDateInfo, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public String apply(DeclaratiePrelucrareDateInfo source) {
						return null;
					}
				}, new Setter<DeclaratiePrelucrareDateInfo, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public void accept(DeclaratiePrelucrareDateInfo bean, String fieldvalue) {

					}
				});

		localitateNastere.setErrorMessage("Localitate Nastere Required !");
		localitateNastere.setWidth("100%");
		localitateNastere.setMaxLength(25);
		localitateNastere.setMinWidth(20f, Unit.EM);
		infoPart311.add(localitateNastere);
		infoLayout1.add(infoPart311);
		infoLayout1.setAlignItems(Alignment.CENTER);

		infoPart31 = new VerticalLayout();
		localitate = new TextField("Localitate Domiciliu: ");
		localitate.setRequiredIndicatorVisible(true);

		binder.forField(localitate).asRequired().bind(new ValueProvider<DeclaratiePrelucrareDateInfo, String>() {

			private static final long serialVersionUID = 1L;

			@Override
			public String apply(DeclaratiePrelucrareDateInfo source) {
				return null;
			}
		}, new Setter<DeclaratiePrelucrareDateInfo, String>() {

			private static final long serialVersionUID = 1L;

			@Override
			public void accept(DeclaratiePrelucrareDateInfo bean, String fieldvalue) {

			}
		});

		localitate.setErrorMessage("Localitate Domiciliu Required !");
		localitate.setWidth("100%");
		localitate.setMaxLength(25);
		localitate.setMinWidth(20f, Unit.EM);
		infoPart31.add(localitate);
		infoLayout1.add(infoPart31);
		infoLayout1.setAlignItems(Alignment.CENTER);

		infoPart41 = new VerticalLayout();
		stradaField = new TextField("Nume Strada:");
		stradaField.setRequiredIndicatorVisible(true);
		infoPart41.add(stradaField);
		infoLayout1.add(infoPart41);
		infoLayout1.setAlignItems(Alignment.CENTER);

		infoLayout2 = new HorizontalLayout();
		infoLayout2.setVisible(false);

		infoPart4 = new VerticalLayout();
		nrStrField = new TextField("Numar Casa:");
		nrStrField.setRequiredIndicatorVisible(true);

		binder.forField(nrStrField).asRequired()
				.withValidator(str -> str.matches("^(\\d){1,4}[/]{0,1}[a-zA-Z]{0,1}$"), "Numar de casa invalid !")
				.bind(new ValueProvider<DeclaratiePrelucrareDateInfo, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public String apply(DeclaratiePrelucrareDateInfo source) {
						return null;
					}
				}, new Setter<DeclaratiePrelucrareDateInfo, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public void accept(DeclaratiePrelucrareDateInfo bean, String fieldvalue) {

					}
				});

		nrStrField.setErrorMessage("Numar de casa required !");
		infoPart4.add(nrStrField);
		infoLayout2.add(infoPart4);
		infoLayout2.setAlignItems(Alignment.CENTER);

		infoPart6 = new VerticalLayout();
		blocField = new TextField("Bloc:");
		infoPart6.add(blocField);
		infoLayout2.add(infoPart6);
		infoLayout2.setAlignItems(Alignment.CENTER);

		infoPart7 = new VerticalLayout();
		scaraField = new TextField("Scara:");
		infoPart7.add(scaraField);
		infoLayout2.add(infoPart7);
		infoLayout2.setAlignItems(Alignment.CENTER);

		infoPart9 = new VerticalLayout();
		apartamentField = new TextField("Apartament:");
		infoPart9.add(apartamentField);
		infoLayout2.add(infoPart9);
		infoLayout2.setAlignItems(Alignment.CENTER);

		infoPart10 = new VerticalLayout();
		(judetField = new TextField("Judet/Sector:")).setRequiredIndicatorVisible(true);

		binder.forField(judetField).asRequired()

				.bind(new ValueProvider<DeclaratiePrelucrareDateInfo, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public String apply(DeclaratiePrelucrareDateInfo source) {
						return null;
					}
				}, new Setter<DeclaratiePrelucrareDateInfo, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public void accept(DeclaratiePrelucrareDateInfo bean, String fieldvalue) {

					}
				});

		judetField.setErrorMessage("Judet Required !");
		infoPart10.add(judetField);
		infoLayout2.add(infoPart10);
		infoLayout2.setAlignItems(Alignment.CENTER);

		infoLayout3 = new HorizontalLayout();
		infoLayout3.setVisible(false);

		infoPart12 = new VerticalLayout();
		(serieField = new TextField("Serie:")).setRequiredIndicatorVisible(true);

		binder.forField(serieField).asRequired()
				.withValidator(str -> str.toString().length() == 2, "Seria are 2 caractere")
				.bind(new ValueProvider<DeclaratiePrelucrareDateInfo, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public String apply(DeclaratiePrelucrareDateInfo source) {
						return null;
					}
				}, new Setter<DeclaratiePrelucrareDateInfo, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public void accept(DeclaratiePrelucrareDateInfo bean, String fieldvalue) {

					}
				});

		serieField.setErrorMessage("Serie ID Required !");
		infoPart12.add(serieField);
		infoLayout3.add(infoPart12);
		infoLayout3.setAlignItems(Alignment.CENTER);

		infoPart13 = new VerticalLayout();
		(nrSerieField = new TextField("Numar C.I:")).setRequiredIndicatorVisible(true);

		binder.forField(nrSerieField).asRequired()
				.withValidator(str -> str.toString().length() == 6, "Seria poate avea 6 caractere")
				.withConverter(new StringToIntegerConverter("Must be Integer"))
				.bind(new ValueProvider<DeclaratiePrelucrareDateInfo, Integer>() {

					private static final long serialVersionUID = 1L;

					@Override
					public Integer apply(DeclaratiePrelucrareDateInfo source) {
						return null;
					}
				}, new Setter<DeclaratiePrelucrareDateInfo, Integer>() {

					private static final long serialVersionUID = 1L;

					@Override
					public void accept(DeclaratiePrelucrareDateInfo bean, Integer fieldvalue) {

					}
				});

		nrSerieField.setErrorMessage("Nr. C.I Required !");
		infoPart13.add(nrSerieField);
		infoLayout3.add(infoPart13);
		infoLayout3.setAlignItems(Alignment.CENTER);

		infoPart14 = new VerticalLayout();
		(eliberatDeField = new TextField("C.I eliberat de:")).setRequiredIndicatorVisible(true);

		binder.forField(eliberatDeField).asRequired()

				.bind(new ValueProvider<DeclaratiePrelucrareDateInfo, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public String apply(DeclaratiePrelucrareDateInfo source) {
						return null;
					}
				}, new Setter<DeclaratiePrelucrareDateInfo, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public void accept(DeclaratiePrelucrareDateInfo bean, String fieldvalue) {

					}
				});

		eliberatDeField.setErrorMessage("Autoritate eliberare C.I Required !");
		infoPart14.add(eliberatDeField);
		infoLayout3.add(infoPart14);
		infoLayout3.setAlignItems(Alignment.CENTER);

		infoPart15 = new VerticalLayout();
		dataEliberareField = new DatePicker("Data eliberare C.I. :");
		dataEliberareField.setLocale(Locale.getDefault());
		dataEliberareField.setI18n(singleFormatI18n);
		dataEliberareField.setRequiredIndicatorVisible(true);

		binder.forField(dataEliberareField).asRequired()

				.bind(new ValueProvider<DeclaratiePrelucrareDateInfo, LocalDate>() {

					private static final long serialVersionUID = 1L;

					@Override
					public LocalDate apply(DeclaratiePrelucrareDateInfo source) {
						return null;
					}
				}, new Setter<DeclaratiePrelucrareDateInfo, LocalDate>() {

					private static final long serialVersionUID = 1L;

					@Override
					public void accept(DeclaratiePrelucrareDateInfo bean, LocalDate fieldvalue) {

					}
				});

		dataEliberareField.setErrorMessage("Data Eliberare C.I Required !");
		infoPart15.add(dataEliberareField);
		infoLayout3.add(infoPart15);
		infoLayout3.setAlignItems(Alignment.CENTER);

		infoLayout4 = new HorizontalLayout();
		infoLayout4.setVisible(false);

		infoPart16 = new VerticalLayout();
		emailField = new EmailField("E-mail: ");
		infoPart16.add(emailField);
		infoLayout4.add(infoPart16);
		infoLayout4.setAlignItems(Alignment.CENTER);

		infoPart17 = new VerticalLayout();
		nrTelefon = new TextField("Nr. Telefon:");
		nrTelefon.setRequiredIndicatorVisible(true);

		binder.forField(nrTelefon).asRequired()
				.withValidator(str -> str.matches("^([00]{0,1}[\\d]{1,3}[0-9]{9,12})$"), "Numar de telefon invalid !")
				.withConverter(new StringToBigIntegerConverter("Nr. telefon poate contine doar cifre"))

				.bind(new ValueProvider<DeclaratiePrelucrareDateInfo, BigInteger>() {

					private static final long serialVersionUID = 1L;

					@Override
					public BigInteger apply(DeclaratiePrelucrareDateInfo source) {
						return null;
					}
				}, new Setter<DeclaratiePrelucrareDateInfo, BigInteger>() {

					private static final long serialVersionUID = 1L;

					@Override
					public void accept(DeclaratiePrelucrareDateInfo bean, BigInteger fieldvalue) {

					}
				});

		nrTelefon.setErrorMessage("Telefon Required !");
		infoPart17.add(nrTelefon);
		infoLayout4.add(infoPart17);
		infoLayout4.setAlignItems(Alignment.CENTER);

		infoPart18 = new VerticalLayout();
		AdresaCorespondenta = new ComboBox<String>("Adresa de corespondenta: ");
		AdresaCorespondenta.setItems("Aceiasi precum domiciliul", "Alta");
		AdresaCorespondenta.setValue("Aceiasi precum domiciliul");

		infoPart18.add(AdresaCorespondenta);
		infoLayout4.add(infoPart18);
		infoLayout4.setAlignItems(Alignment.CENTER);

		AdresaCorespondenta.addValueChangeListener(eventAC -> {
			if (complete.getValue())
				if (AdresaCorespondenta.getValue().equals("Alta")) {
					infoPart19.setVisible(true);

				} else {
					infoPart19.setVisible(false);
				}

		});

		infoPart19 = new VerticalLayout();
		infoPart19.setVisible(false);
		AdresaCorespondentaAlternativa = new TextField("Adresa Corespondenta Alternativa: ");
		infoPart19.add(AdresaCorespondentaAlternativa);
		infoLayout4.add(infoPart19);
		infoLayout4.setAlignItems(Alignment.CENTER);

		content.add(infoLayout);
		content.setAlignItems(Alignment.CENTER);
		content.add(infoLayout1);
		content.setAlignItems(Alignment.CENTER);
		content.add(infoLayout2);
		content.setAlignItems(Alignment.CENTER);
		content.add(infoLayout3);
		content.setAlignItems(Alignment.CENTER);
		content.add(infoLayout4);
		content.setAlignItems(Alignment.CENTER);

		generateLayout = new HorizontalLayout();
		generate = new Button("Generate", VaadinIcon.FILE_PROCESS.create());
		generate.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
		generate.getClassNames().add("frendly");
		generate.addClickListener(evt -> {

			if (complete.getValue()) {
				map.put("titlu", titlu.getValue());
				map.put("nume", PDFHelper.capitalizeWords(numeField.getValue().trim()));
				map.put("cnp", cnpField.getValue().trim());
				map.put("dataNastere", dataNasteriiField.getValue().toString());
				map.put("localitateNastere", localitateNastere.getValue().trim());
				map.put("domiciliu", localitate.getValue());
				map.put("strada", stradaField.getValue().trim());
				map.put("nrCasaAddr", nrStrField.getValue().trim());
				map.put("bloc", blocField.getValue().trim());

				map.put("scara", scaraField.getValue().trim());

				map.put("apartament", apartamentField.getValue().trim());
				map.put("judet", judetField.getValue().trim());
				map.put("serie", serieField.getValue().trim());
				map.put("nrci", nrSerieField.getValue().trim());
				map.put("cieliberat", eliberatDeField.getValue().trim());
				map.put("dataci", dataEliberareField.getValue().toString());
				map.put("email", emailField.getValue().trim());
				map.put("telefon", nrTelefon.getValue().trim());
				map.put("adresaAlternativa", AdresaCorespondentaAlternativa.getValue().trim());

			}
			numeField.clear();
			nrStrField.clear();
			cnpField.clear();
			dataNasteriiField.clear();
			localitateNastere.clear();
			localitate.clear();
			nrStrField.clear();
			nrStrField.clear();
			blocField.clear();
			scaraField.clear();
			apartamentField.clear();
			judetField.clear();
			serieField.clear();
			nrSerieField.clear();
			eliberatDeField.clear();
			dataEliberareField.clear();
			emailField.clear();
			nrTelefon.clear();

			DeclaratiePrelucrareDateCreator pdfcr = new DeclaratiePrelucrareDateCreator(map, Utils.getTimeStr());
			String fn = pdfcr.getID();

			RouteConfiguration.forSessionScope().removeRoute(DeclaratiePrelucrareDatePDF.class);
			RouteConfiguration.forSessionScope().setRoute(DeclaratiePrelucrareDatePDF.NAME,
					DeclaratiePrelucrareDatePDF.class);
			Map<String, String> sss = new HashMap<String, String>();
			sss.put("tm", fn);

			UI.getCurrent().navigate("DeclaratiePrelucrareDatePDF", QueryParameters.simple(sss));
		});
		generateLayout.add(generate);

		binder.addStatusChangeListener(event -> {

			if (!complete.getValue() || binder.isValid()) {
				generate.setEnabled(true);
			} else {
				generate.setEnabled(false);
			}
		});

		content.add(generateLayout);
		content.setAlignItems(Alignment.CENTER);
		content.setMargin(false);
		add(content);
	}

	private void toggleVisibility() {
		infoLayout.setVisible(!infoLayout.isVisible());
		infoLayout1.setVisible(!infoLayout1.isVisible());
		infoLayout2.setVisible(!infoLayout2.isVisible());
		infoLayout3.setVisible(!infoLayout3.isVisible());
		infoLayout4.setVisible(!infoLayout4.isVisible());
		generate.setEnabled(!generate.isEnabled());
	}

	@Override
	public void afterNavigation(AfterNavigationEvent event) {
		RouteConfiguration.forSessionScope().removeRoute(NAME);
	}
}
