package com.spectral369.birotica;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.ErrorParameter;
import com.vaadin.flow.router.NotFoundException;
import com.vaadin.flow.router.ParentLayout;
import com.vaadin.flow.router.QueryParameters;
import com.vaadin.flow.router.RouteNotFoundError;
import com.vaadin.flow.router.RouteParameters;

import jakarta.servlet.http.HttpServletResponse;

@ParentLayout(MainView.class)
public class CustomNotFoundTarget extends RouteNotFoundError {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3039412172140255224L;

	@Override
	public int setErrorParameter(BeforeEnterEvent event, ErrorParameter<NotFoundException> parameter) {
		//System.out.println("not found " + event.getLocation().getPath() + " " + parameter.getCaughtException());
		Notification.show("Path Not available!", 3000, Position.BOTTOM_END);
	
		Map<String, List<String>> map =  new HashMap<>();
		QueryParameters qp =  new QueryParameters(map);
		RouteParameters rp =  new RouteParameters("", "");
		event.forwardTo(MainView.class, rp, qp);
		return HttpServletResponse.SC_NOT_FOUND;
	}
	
}