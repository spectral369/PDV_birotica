package com.spectral369.DDIS;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.event.PdfDocumentEvent;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Tab;
import com.itextpdf.layout.element.TabStop;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.element.Text;
import com.itextpdf.layout.properties.HorizontalAlignment;
import com.itextpdf.layout.properties.TabAlignment;
import com.itextpdf.layout.properties.TextAlignment;
import com.itextpdf.layout.properties.UnitValue;
import com.itextpdf.layout.properties.VerticalAlignment;
import com.spectral369.birotica.PdfList;
import com.spectral369.utils.FooterEvt;
import com.spectral369.utils.PDFHelper;
import com.spectral369.utils.Utils;

public class DeclaratieDeImpunereSalubrizareCreator {

	File pdff = null;
	private transient PdfDocument document;
	private transient PdfWriter writer;
	String id = Utils.getRandomStringSerial("DDIS");

	public DeclaratieDeImpunereSalubrizareCreator(Map<String, String> map, String tm) {

		pdff = new File(Utils.getSaveFileLocation("Declaratie_de_impunere_salubrizare" + tm + ".pdf"));
		if (map.isEmpty()) {
			generatePDF(tm, pdff);
			PdfList.addFile(id, pdff.getAbsolutePath());

		} else {

			generatePDF(tm, pdff, map);
			PdfList.addFile(id, pdff.getAbsolutePath());
		}
	}

	public String getID() {
		return this.id;
	}

	private void generatePDF(final String tm, final File pdfFile) {
		try {
			writer = new PdfWriter(pdfFile);

			document = new PdfDocument(writer);
			document.getDocumentInfo().addCreationDate();
			document.getDocumentInfo().setAuthor("spectral369");
			document.getDocumentInfo().setTitle("DDIS_" + tm);
			document.setDefaultPageSize(PageSize.A4);
			Document doc = new Document(document);
			Image antetLogo = PDFHelper.getAntetLogo();
			doc.setFontSize(11.5f);
			float width = doc.getPageEffectiveArea(PageSize.A4).getWidth();
			float marginLeft = doc.getLeftMargin();
			float marginRIght = doc.getRightMargin();
			document.addEventHandler(PdfDocumentEvent.END_PAGE, new FooterEvt(width + (marginLeft + marginRIght)));

			Paragraph antet = new Paragraph();
			antet.add(PDFHelper.getEmptySpace(5));

			float documentWidth = ((document.getDefaultPageSize().getWidth() - doc.getLeftMargin()
					- doc.getRightMargin()) * 0.96f);
			float documentHeight = ((document.getDefaultPageSize().getHeight() - doc.getTopMargin()
					- doc.getBottomMargin()) * 0.96f);

			antetLogo.scaleToFit(documentWidth, documentHeight);

			antet.add(antetLogo);
			antet.setHorizontalAlignment(HorizontalAlignment.CENTER);
			doc.add(antet);

			final Paragraph nrInreg = new Paragraph();

			// nrInreg.add(new Tab());
			Text nrI = new Text("\nDECLARATIE DE\n IMPUNERE\n").simulateBold();
			nrI.setFontSize(15f);
			Text nrII = new Text(
					"in vederea stabilirii cuantumului taxei speciale de salubrizare pentru\n utilizatorii casnici");
			nrInreg.add(nrI);
			nrInreg.add(nrII);
			nrInreg.setTextAlignment(TextAlignment.CENTER);
			doc.add(nrInreg);

			final Paragraph declaratie = new Paragraph();
			declaratie.add(PDFHelper.addTab());
			Text dec1 = new Text("Subsemnatul(a) " + PDFHelper.getStrWithDash(38, "")
					+ "avand calitate de proprietar\nchirias/concesionar/locatar/titular al dreptului"
					+ " de administrare sau de folosinta (in vazul locuintelor proprietate de stat/UAT), al locuintei situata in localitatea "
					+ PDFHelper.getStrWithDash(22, "") + " strada " + PDFHelper.getStrWithDash(18, "") + ", nr."
					+ PDFHelper.getStrWithDash(7, "") + ", bl." + PDFHelper.getStrWithDash(4, "") + ", sc."
					+ PDFHelper.getStrWithDash(4, "") + ", ap." + PDFHelper.getStrWithDash(4, "")
					+ ", domiciliat(a) in \nlocalitatea " + PDFHelper.getStrWithDash(22, "") + ", str. "
					+ PDFHelper.getStrWithDash(18, "") + ", nr." + PDFHelper.getStrWithDash(7, "") + ", bl."
					+ PDFHelper.getStrWithDash(4, "") + ", sc." + PDFHelper.getStrWithDash(4, "") + ", ap."
					+ PDFHelper.getStrWithDash(4, "") + ", posesor al CI seria " + PDFHelper.getStrWithDash(5, "")
					+ ", numarul " + PDFHelper.getStrWithDash(9, "") + ", C.N.P " + PDFHelper.getStrWithDash(20, "")
					+ ", avand locul de munca la/pensionar " + PDFHelper.getStrWithDash(22, "")
					+ ", declar pe propria raspundere ca unitatea locativa are in componenta urmatorii membrii "
					+ "(locatari stabili, chiriasi, flotanti):\n\n");
			dec1.setFontSize(11.5f);
			declaratie.add(dec1);
			doc.add(declaratie);

			Table table = new Table(UnitValue.createPercentArray(new float[] { 4.5f, 2.5f, 3 }));
			table.setWidth(UnitValue.createPercentValue(96));
			table.setFixedLayout();
			table.setHorizontalAlignment(HorizontalAlignment.CENTER);
			table.setVerticalAlignment(VerticalAlignment.MIDDLE);
			table.setTextAlignment(TextAlignment.CENTER);

			Cell cell = new Cell();
			cell.add(new Paragraph("Numele si prenumele"));
			cell.setTextAlignment(TextAlignment.CENTER);
			cell.setHorizontalAlignment(HorizontalAlignment.CENTER);
			table.addCell(cell);

			cell = new Cell();
			cell.add(new Paragraph("Calitatea(locatari stabili, chiriasi, flotanti)"));
			cell.setTextAlignment(TextAlignment.CENTER);
			cell.setHorizontalAlignment(HorizontalAlignment.CENTER);

			table.addCell(cell);

			cell = new Cell();
			cell.add(new Paragraph("Observatii"));
			cell.setTextAlignment(TextAlignment.CENTER);
			cell.setHorizontalAlignment(HorizontalAlignment.CENTER);
			table.addCell(cell);

			// row 1
			for (int i = 0; i < 6; i++) {
				cell = new Cell();
				cell.add(new Paragraph(" ").setFontSize(15f));
				cell.setTextAlignment(TextAlignment.CENTER);
				cell.setHeight(13f);
				cell.setHorizontalAlignment(HorizontalAlignment.CENTER);
				table.addCell(cell);

				cell = new Cell();
				cell.add(new Paragraph(" ").setFontSize(15f));
				cell.setTextAlignment(TextAlignment.CENTER);
				cell.setHorizontalAlignment(HorizontalAlignment.CENTER);
				cell.setHeight(13f);
				table.addCell(cell);

				cell = new Cell();
				cell.add(new Paragraph(" ").setFontSize(15f));
				cell.setTextAlignment(TextAlignment.CENTER);
				cell.setHorizontalAlignment(HorizontalAlignment.CENTER);
				cell.setHeight(13f);
				table.addCell(cell);
			}

			doc.add(table);
			final Paragraph declaratie2 = new Paragraph();
			declaratie2.add(PDFHelper.addTab());
			Text dec12 = new Text(
					"Se  vor trece IN TABEL datele membrilor de familie/locatarilor, inclusiv cele ale persoanei care "
							+ "completeaza declaratia de impunere ( daca domiciliaza la adresa mentionata). Pentru locuintele inchiriate persoanelor fizice se vor trece datele tuturor persoanelor "
							+ "care locuiesc la adresa mentionata.\n\n");
			dec12.setFontSize(10.7f);
			declaratie2.add(dec12);

			declaratie2.add(PDFHelper.addTab());
			Text dec13 = new Text(
					"Declar pe propria raspundere, cunoscand prevederile art. 326 Cod Penal referitoare la falsul in declaratii, ca toate datele furnizate "
							+ "in acesta declaratie precum si documenteleatasate sunt conforme cu realitatea.\n\n");
			dec12.setFontSize(10.7f);
			declaratie2.add(dec13);

			doc.add(declaratie2);
			Table semnaturi = new Table(1);

			semnaturi.setHorizontalAlignment(HorizontalAlignment.CENTER);
			Paragraph p1 = new Paragraph();
			Text primar = new Text("Data");
			p1.add(primar);
			p1.addTabStops(new TabStop(width / 1.18f, TabAlignment.RIGHT));
			p1.add(new Tab());
			Text intocmit = new Text("Semnatura");
			p1.add(intocmit);
			Cell cell1 = new Cell();
			cell1.setBorder(Border.NO_BORDER);
			cell1.add(p1);
			semnaturi.addCell(cell1);
			doc.add(semnaturi);

			Table semnaturiR = new Table(1);

			semnaturiR.setHorizontalAlignment(HorizontalAlignment.CENTER);
			Paragraph semnR = new Paragraph();
			// String dateNow = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
			Text semnPrim = new Text(PDFHelper.getStrWithDash(22, ""));
			semnR.add(semnPrim);
			semnR.addTabStops(new TabStop(width / 1.18f, TabAlignment.RIGHT));
			semnR.add(new Tab());
			Text semnIntocmit = new Text(PDFHelper.getStrWithDash(22, ""));
			semnR.add(semnIntocmit);
			Cell cell2 = new Cell();
			cell2.setBorder(Border.NO_BORDER);
			cell2.add(semnR);
			semnaturiR.addCell(cell2);
			doc.add(semnaturiR);
			// next

			Table datefooter = new Table(1);

			datefooter.setHorizontalAlignment(HorizontalAlignment.CENTER);
			Paragraph p3 = new Paragraph();
			Text telefon = new Text("Telefon");
			p3.add(telefon);
			p3.addTabStops(new TabStop(width / 1.18f, TabAlignment.RIGHT));
			p3.add(new Tab());
			Text email = new Text("E-mail");
			p3.add(email);
			Cell cell3 = new Cell();
			cell3.setBorder(Border.NO_BORDER);
			cell3.add(p3);
			datefooter.addCell(cell3);
			doc.add(datefooter);

			Table datefooterR = new Table(1);

			datefooterR.setHorizontalAlignment(HorizontalAlignment.CENTER);
			Paragraph footerR = new Paragraph();
			Text telefonR = new Text(PDFHelper.getStrWithDash(22, ""));
			footerR.add(telefonR);
			footerR.addTabStops(new TabStop(width / 1.18f, TabAlignment.RIGHT));
			footerR.add(new Tab());
			Text emailR = new Text(PDFHelper.getStrWithDash(22, ""));
			footerR.add(emailR);
			Cell cell4 = new Cell();
			cell4.setBorder(Border.NO_BORDER);
			cell4.add(footerR);
			datefooterR.addCell(cell4);
			doc.add(datefooterR);

			doc.close();
			document.close();
			writer.flush();

		} catch (IOException ex2) {
		}

	}

	private void generatePDF(final String tm, final File pdfFile, final Map<String, String> map) {
		try {
			writer = new PdfWriter(pdfFile);

			document = new PdfDocument(writer);
			document.getDocumentInfo().addCreationDate();
			document.getDocumentInfo().setAuthor("spectral369");
			document.getDocumentInfo().setTitle("CSPH_" + tm);
			document.setDefaultPageSize(PageSize.A4);
			Document doc = new Document(document);
			doc.setFontSize(11.5f);
			float width = doc.getPageEffectiveArea(PageSize.A4).getWidth();
			float marginLeft = doc.getLeftMargin();
			float marginRIght = doc.getRightMargin();
			document.addEventHandler(PdfDocumentEvent.END_PAGE, new FooterEvt(width + (marginLeft + marginRIght)));

			final Paragraph nrInreg = new Paragraph();

			// nrInreg.add(new Tab());
			Text nrI = new Text("CERERE SCUTIRE PENTRU PERSOANELE \n" + "CU GRAD DE HANDICAP Nr. "
					+ PDFHelper.getStrWithDots(15, "") + " " + "data " + PDFHelper.getStrWithDots(20, "") + "\n\n")
					.simulateBold();
			nrInreg.add(nrI);
			nrInreg.setTextAlignment(TextAlignment.CENTER);
			doc.add(nrInreg);

			final Paragraph declaratie = new Paragraph();
			declaratie.add(PDFHelper.addTab());
			final String titlu = map.get("titlu").toLowerCase().contains("dl") ? "Subsemnatul " : "Subsemnata ";
			final String domiciliu = map.get("titlu").toLowerCase().contains("dl") ? "domiciliat " : "domiciliata: ";
			final String legitimat = map.get("titlu").toLowerCase().contains("dl") ? "legitimat " : "legitimata ";
			declaratie.add(titlu);
			declaratie.add(PDFHelper
					.createAdjustableParagraph(50,
							new Paragraph(map.get("nume")).simulateBold().setTextAlignment(TextAlignment.CENTER))
					.setFixedLeading(11));
			declaratie.add(" " + domiciliu + "in ");
			declaratie.add(PDFHelper
					.createAdjustableParagraph(60, new Paragraph(map.get("localitate") + " Nr." + map.get("nrStrada"))
							.simulateBold().setTextAlignment(TextAlignment.CENTER))
					.setFixedLeading(11));
			declaratie.add(" " + legitimat + "cu");
			declaratie.add(new Text(" C.I ").simulateBold());
			declaratie.add(" seria ");
			declaratie.add(PDFHelper
					.createAdjustableParagraph(10,
							new Paragraph(map.get("serie")).simulateBold().setTextAlignment(TextAlignment.CENTER))
					.setFixedLeading(11));
			declaratie.add(" nr. ");
			declaratie.add(PDFHelper
					.createAdjustableParagraph(13,
							new Paragraph(map.get("nrSerie")).simulateBold().setTextAlignment(TextAlignment.CENTER))
					.setFixedLeading(11));
			declaratie.add(" avand C.N P ");
			declaratie.add(PDFHelper
					.createAdjustableParagraph(38,
							new Paragraph(map.get("cnp")).simulateBold().setTextAlignment(TextAlignment.CENTER))
					.setFixedLeading(11));
			declaratie.add(" telefon ");
			declaratie.add(PDFHelper
					.createAdjustableParagraph(18,
							new Paragraph(map.get("telefon")).simulateBold().setTextAlignment(TextAlignment.CENTER))
					.setFixedLeading(11));

			Paragraph dec11 = new Paragraph(
					" solicit scutirea impozitului/taxei pe cladire si teren<curti constructii> la imobilul de domiciliu si mijlocul de transport"
							+ " marca ");
			dec11.add(PDFHelper
					.createAdjustableParagraph(20,
							new Paragraph(map.get("marcaAuto")).simulateBold().setTextAlignment(TextAlignment.CENTER))
					.setFixedLeading(11));
			dec11.add(" conform art.456 alin. 1 lit. t; art. 454 alin. 1 lit. t, si/sau art. 469 alin. 1 lit. b din "
					+ "Legea Nr. 227/2015 privind Codul Fiscal cu modificarile si completarile ulterioare. Declar ca imobilul ");
			// declaratie.add(dec11);
			Text dec11a = new Text("[este] / [nu este] ").setUnderline();
			dec11.add(dec11a);
			Text dec11b = new Text("folosit in scop economic sau de agrement.\n");
			dec11.add(dec11b);
			declaratie.add(dec11);
			declaratie.add(PDFHelper.addTab());
			Text dec111 = new Text(
					"Mentionez ca sunt(dupa caz) : [  ] persoana cu handicap grav sau accentuat; [  ] persoana incadrata in gradul I de invaliditate; "
							+ "[  ] reprezentant legal al persoanei cu handicap grav/ accentuat sau incadrata im grad I de invaliditate.\n");
			declaratie.add(dec111);

			declaratie.add(PDFHelper.addTab());
			Text dec12 = new Text(
					"Anexez prezentei copii ale documentelor justificative, certificate in conformitate cu originalul:\n");
			declaratie.add(dec12);
			Text anex01 = new Text(" [  ] - Buletin / carte de indentitate(proprietar/coproprietar).\n");
			declaratie.add(anex01);
			Text anex02 = new Text(" [  ] - Act proprietate / coproprietate.\n");
			declaratie.add(anex02);
			Text anex03 = new Text(" [  ] - Certificat de incadrare in grad de handicap.\n");
			declaratie.add(anex03);
			Text anex04 = new Text(
					" [  ] - Hotararea Judecatoreasca definitiva de instituire a reprezentantului legal(dupa caz).\n");
			declaratie.add(anex04);
			Text anex05 = new Text(" [  ] - Cartea de Indentitate a autovehiculului.\n");
			declaratie.add(anex05);
			Text anex06 = new Text(
					" [  ] - Certificat de nastere minor, carte de indentitate parinti / reprezentant legal.\n");
			declaratie.add(anex06);
			Text anex07 = new Text(" [  ] - contract de inchiriere / comodat pentru suprafata de "
					+ PDFHelper.getStrWithDots(15, "") + " mp. (dupa caz).\n\n");
			declaratie.add(anex07);

			declaratie.add(PDFHelper.addTab());
			Text dec13 = new Text(
					"Scutirea se acorda integral pentru cladirea folosita ca domiciliu(rezidenta), terenul din categoria curti-contructii aferent acestia "
							+ "si un singur mijloc de transport la alegere, aflate in proprietatea sau coproprietatea (sot/sotie), persoanelor cu handicap grav sau accentuat si a persoanelor"
							+ " incadrate in gradul I de invaliditate, respectiv a reprezentantilor legali, pe perioada in care au in ingrijire, supraveghere si intretinere persoane cu"
							+ " handicap grav sau accentuat si persoane in gradul I de invaliditate. In situatia in care o cota-parte apartine unor terti, scutirea nu se acorda pentru"
							+ " cota-parte detinuta de acestea.\n");
			dec13.setFontSize(9.5f);
			declaratie.add(dec13);
			declaratie.add(PDFHelper.addTab());
			Text dec14 = new Text(
					"Scutirea de la plata impozitului / taxei  se acorda incepand cu data de 1 a lunii urmatoare depunerii cererii, pe baza "
							+ "documentelor justificative, certificate in conformitate cu orginalul(art.64 alin. 5 din Legea nr.207/2015 priviind Codul de Procedura Fiscala), care atesta "
							+ "situatia respectiva, cu exceptia contribuabililor care sunt deja inscrisi in baza de data a U.A.T Dudestii-vechi.\n");
			dec14.setFontSize(9.5f);
			declaratie.add(dec14);
			declaratie.add(PDFHelper.addTab());
			Text dec15 = new Text(
					"Daca intervin schimbari care conduc la modificarea conditiilor in care se acorda scutirile de impozite / taxe, persoanele in cauza trebuie sa depuna "
							+ "noi declaratii fiscale in termen de 45 de zile de la aparitia schimbarilor.\n");
			dec15.setFontSize(9.5f);
			declaratie.add(dec15);
			declaratie.add(PDFHelper.addTab());
			Text dec2 = new Text("Subsemnatul/a " + PDFHelper.getStrWithDots(52, "")
					+ " ,declar ca sunt de acord si imi"
					+ " exprim consimtamantul in mod express, neechivoc, liber si informat cu privire la prelucrarea datelor "
					+ "mele cu caracter personal, conform prevederilor Regulamentului(U.E) 679/2016 privind protectia "
					+ "persoanelor fizice in ceea ce priveste prelucrarea datelor cu caracter personal si privind libera "
					+ "circulatie a acestora, pentru a fi colectate, folosite si prelucrate (nume, prenume, C.N.P, adresa "
					+ "postala, adresa de e-mail, nr. de telefon, copie carte de indentitate, componenta familiei, extras "
					+ "de cont bancar, etc) de catre U.A.T Dudestii-Vechi in vederea indeplinirilor atributiilor legale ale "
					+ " acestei institutii .\n");
			dec2.setFontSize(9.5f);
			declaratie.add(dec2);
			declaratie.add(PDFHelper.addTab());
			Text dec3 = new Text("Am luat la cunostiinta de drepturile legale pe care le am odata cu prelucrarea, "
					+ "colectarea si folosirea datelor cu caracter personal conform informarii comunicate de catre operator.\n\n\n");
			dec3.setFontSize(9.5f);
			declaratie.add(dec3);
			doc.add(declaratie);

			Table semnaturi = new Table(1);

			semnaturi.setHorizontalAlignment(HorizontalAlignment.CENTER);
			Paragraph p1 = new Paragraph();
			Text primar = new Text("Data");
			p1.add(primar);
			p1.addTabStops(new TabStop(width / 1.18f, TabAlignment.RIGHT));
			p1.add(new Tab());
			Text intocmit = new Text("Semnatura");
			p1.add(intocmit);
			Cell cell1 = new Cell();
			cell1.setBorder(Border.NO_BORDER);
			cell1.add(p1);
			semnaturi.addCell(cell1);
			doc.add(semnaturi);

			Table semnaturiR = new Table(1);

			semnaturiR.setHorizontalAlignment(HorizontalAlignment.CENTER);
			Paragraph semnR = new Paragraph();
			// String dateNow = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
			Text semnPrim = new Text(PDFHelper.getStrWithDash(22, ""));
			semnR.add(semnPrim);
			semnR.addTabStops(new TabStop(width / 1.18f, TabAlignment.RIGHT));
			semnR.add(new Tab());
			Text semnIntocmit = new Text(PDFHelper.getStrWithDash(22, ""));
			semnR.add(semnIntocmit);
			Cell cell2 = new Cell();
			cell2.setBorder(Border.NO_BORDER);
			cell2.add(semnR);
			semnaturiR.addCell(cell2);
			doc.add(semnaturiR);

			doc.close();
			document.close();
			writer.flush();

		} catch (IOException ex2) {
		}

	}

}
