package com.spectral369.DDIS;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.spectral369.birotica.MainView;
import com.spectral369.utils.PDFHelper;
import com.spectral369.utils.Utils;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.Notification.Position;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.Setter;
import com.vaadin.flow.data.converter.StringToBigIntegerConverter;
import com.vaadin.flow.data.converter.StringToIntegerConverter;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.function.ValueProvider;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;
import com.vaadin.flow.router.QueryParameters;
import com.vaadin.flow.router.RouteConfiguration;
import com.vaadin.flow.router.RouterLayout;

public class DeclaratieDeImpunereSalubrizareInfo extends HorizontalLayout
		implements RouterLayout, AfterNavigationObserver {
	private static final long serialVersionUID = 1L;
	public static final String NAME = "DeclaratieDeImpunereSalubrizareInfo";
	VerticalLayout content;
	HorizontalLayout backLayout;
	Button backbtn;
	HorizontalLayout titlelLayout;
	Button IarbaTitle;
	HorizontalLayout checkLayout;
	Checkbox complete;
	HorizontalLayout infoLayout;
	VerticalLayout infoPart1;
	VerticalLayout infoPart1_2;
	VerticalLayout infoPart2;
	VerticalLayout infoPart3;
	HorizontalLayout infoLayout1;
	VerticalLayout infoPart4;
	VerticalLayout infoPart5;
	VerticalLayout infoPart6;
	HorizontalLayout infoLayout2;
	VerticalLayout infoPart7;
	VerticalLayout infoPart8;
	VerticalLayout infoPart9;
	VerticalLayout infoPart10;
	VerticalLayout infoPart11;
	VerticalLayout infoPart12;
	HorizontalLayout infoLayout3;
	VerticalLayout infoPart13;
	VerticalLayout infoPart14;
	VerticalLayout infoPart15;
	VerticalLayout infoPart16;
	HorizontalLayout infoLayout4;
	VerticalLayout infoPart17;
	VerticalLayout infoPart18;
	HorizontalLayout infoLayout5;
	VerticalLayout infoPart19;
	VerticalLayout infoPart20;
	VerticalLayout infoPart21;
	VerticalLayout infoPart22;
	HorizontalLayout infoLayout6;
	VerticalLayout infoPart23;

	HorizontalLayout generateLayout;
	Button generate;
	DeclaratieDeImpunereSalubrizarePDF pdf;
	Map<String, String> map;
	private Binder<DeclaratieDeImpunereSalubrizareInfo> binder;
	private TextField numeField;
	private TextField stradaLocuinta;
	private TextField nrLocuinta;
	private TextField BlLocuinta;
	private TextField ScLocuinta;
	private TextField ApLocuinta;
	private TextField localitateLocuinta;
	private TextField localitateDomiciliu;
	private TextField stradaDomiciliu;
	private TextField nrDomiciliu;
	private TextField BlDomiciliu;
	private TextField ScDomiciliu;
	private TextField ApDomiciliu;
	private TextField serieCIField;
	private TextField nrCIField;
	private TextField cnpField;
	private TextField LocDeMuncaField;
	private TextField nrTelefon;
	private EmailField emailField;
	private TextField locuitor;
	private ComboBox<String> calitate;
	private TextField Observatii;
	private Map<Integer, String> pers = new HashMap<>();
	private int persSize = 0;
	private List<Person> persList = new ArrayList<Person>();
	private Grid<Person> grid = new Grid<>(Person.class, false);

	public DeclaratieDeImpunereSalubrizareInfo() {
		content = new VerticalLayout();
		map = new HashMap<String, String>();
		binder = new Binder<DeclaratieDeImpunereSalubrizareInfo>(DeclaratieDeImpunereSalubrizareInfo.class);
		backLayout = new HorizontalLayout();
		backLayout.setMargin(true);
		backbtn = new Button("Back", VaadinIcon.ARROW_CIRCLE_LEFT.create());
		backbtn.getClassNames().add("quiet");
		backbtn.addClickListener(evt -> {
			RouteConfiguration.forSessionScope().removeRoute(NAME);
			UI.getCurrent().navigate(MainView.class);
		});
		backLayout.add(backbtn);
		content.add(backLayout);
		content.setAlignItems(Alignment.START);
		titlelLayout = new HorizontalLayout();
		IarbaTitle = new Button("Declaratie De Impunere Salubrizare", VaadinIcon.FILE_TEXT_O.create());
		IarbaTitle.setEnabled(false);
		IarbaTitle.setId("mainTitle");
		IarbaTitle.addClassName("clearDisabled");
		titlelLayout.add(IarbaTitle);
		content.add(titlelLayout);
		content.setAlignItems(Alignment.CENTER);
		checkLayout = new HorizontalLayout();
		(complete = new Checkbox("Cu completare de date ?!", false)).addValueChangeListener(evt -> {
			toggleVisibility();
		});
		complete.setEnabled(false);
		checkLayout.add(complete);
		content.add(checkLayout);
		content.setAlignItems(Alignment.CENTER);

		infoLayout = new HorizontalLayout();
		infoLayout.setVisible(false);
		infoPart1 = new VerticalLayout();
		numeField = new TextField("Nume complet:");
		numeField.setRequiredIndicatorVisible(true);
		binder.forField(numeField).asRequired()
				.withValidator(str -> str.length() > 4, "Numele sa fie mai mare decat 4 caractere")
				.bind(new ValueProvider<DeclaratieDeImpunereSalubrizareInfo, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public String apply(DeclaratieDeImpunereSalubrizareInfo source) {
						return null;
					}
				}, new Setter<DeclaratieDeImpunereSalubrizareInfo, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public void accept(DeclaratieDeImpunereSalubrizareInfo bean, String fieldvalue) {

					}
				});

		numeField.setErrorMessage("Nume Required");
		numeField.setWidth("100%");
		numeField.setMaxLength(25);
		numeField.setMinWidth(20f, Unit.EM);
		infoPart1.add(numeField);
		infoLayout.add(infoPart1);
		infoLayout.setAlignItems(Alignment.CENTER);

		infoPart1_2 = new VerticalLayout();
		localitateLocuinta = new TextField("Localitate:");
		localitateLocuinta.setValue("Dudestii-Vechi");
		localitateLocuinta.setRequiredIndicatorVisible(true);
		binder.forField(localitateLocuinta).asRequired()
				.withValidator(str -> str.length() > 2, "Numele localitatii sa fie mai mare decat 2 caractere")

				.bind(new ValueProvider<DeclaratieDeImpunereSalubrizareInfo, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public String apply(DeclaratieDeImpunereSalubrizareInfo source) {
						return null;
					}
				}, new Setter<DeclaratieDeImpunereSalubrizareInfo, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public void accept(DeclaratieDeImpunereSalubrizareInfo bean, String fieldvalue) {

					}
				});

		localitateLocuinta.setErrorMessage("Localitate Required !");
		infoPart1_2.add(localitateLocuinta);
		infoLayout.add(infoPart1_2);
		infoLayout.setAlignItems(Alignment.CENTER);

		infoPart2 = new VerticalLayout();
		stradaLocuinta = new TextField("Str. loc:");
		stradaLocuinta.setRequiredIndicatorVisible(true);
		binder.forField(stradaLocuinta).asRequired()
				.withValidator(str -> str.length() > 2, "Numele strazii sa fie mai mare decat 2 caractere")

				.bind(new ValueProvider<DeclaratieDeImpunereSalubrizareInfo, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public String apply(DeclaratieDeImpunereSalubrizareInfo source) {
						return null;
					}
				}, new Setter<DeclaratieDeImpunereSalubrizareInfo, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public void accept(DeclaratieDeImpunereSalubrizareInfo bean, String fieldvalue) {

					}
				});

		stradaLocuinta.setErrorMessage("Strada Required !");
		infoPart2.add(stradaLocuinta);
		infoLayout.add(infoPart2);
		infoLayout.setAlignItems(Alignment.CENTER);

		infoPart3 = new VerticalLayout();
		nrLocuinta = new TextField("Nr. loc:");
		nrLocuinta.setRequiredIndicatorVisible(true);
		binder.forField(nrLocuinta).asRequired().bind(new ValueProvider<DeclaratieDeImpunereSalubrizareInfo, String>() {

			private static final long serialVersionUID = 1L;

			@Override
			public String apply(DeclaratieDeImpunereSalubrizareInfo source) {
				return null;
			}
		}, new Setter<DeclaratieDeImpunereSalubrizareInfo, String>() {

			private static final long serialVersionUID = 1L;

			@Override
			public void accept(DeclaratieDeImpunereSalubrizareInfo bean, String fieldvalue) {

			}
		});

		nrLocuinta.setErrorMessage("Nr. Required !");
		infoPart3.add(nrLocuinta);
		infoLayout.add(infoPart3);
		infoLayout.setAlignItems(Alignment.CENTER);

		infoLayout1 = new HorizontalLayout();
		infoLayout1.setVisible(false);

		infoPart4 = new VerticalLayout();
		BlLocuinta = new TextField("Bl. loc:");
		infoPart4.add(BlLocuinta);
		infoLayout1.add(infoPart4);
		infoLayout1.setAlignItems(Alignment.CENTER);

		infoPart5 = new VerticalLayout();
		ScLocuinta = new TextField("Sc. loc:");
		infoPart5.add(ScLocuinta);
		infoLayout1.add(infoPart5);
		infoLayout1.setAlignItems(Alignment.CENTER);

		infoPart6 = new VerticalLayout();
		ApLocuinta = new TextField("Ap. loc:");
		infoPart6.add(ApLocuinta);
		infoLayout1.add(infoPart6);
		infoLayout1.setAlignItems(Alignment.CENTER);

		infoLayout2 = new HorizontalLayout();
		infoLayout2.setVisible(false);

		infoPart7 = new VerticalLayout();
		localitateDomiciliu = new TextField("Localitate Domiciliu:");
		localitateDomiciliu.setValue("Dudestii-Vechi");
		localitateDomiciliu.setRequiredIndicatorVisible(true);
		binder.forField(localitateDomiciliu).asRequired()
				.withValidator(str -> str.length() > 2, "Numele localitatii sa fie mai mare decat 2 caractere")

				.bind(new ValueProvider<DeclaratieDeImpunereSalubrizareInfo, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public String apply(DeclaratieDeImpunereSalubrizareInfo source) {
						return null;
					}
				}, new Setter<DeclaratieDeImpunereSalubrizareInfo, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public void accept(DeclaratieDeImpunereSalubrizareInfo bean, String fieldvalue) {

					}
				});

		localitateDomiciliu.setErrorMessage("Localitate Required !");
		infoPart7.add(localitateDomiciliu);
		infoLayout2.add(infoPart7);
		infoLayout2.setAlignItems(Alignment.CENTER);

		infoPart8 = new VerticalLayout();
		stradaDomiciliu = new TextField("Str. domiciliu:");
		stradaDomiciliu.setRequiredIndicatorVisible(true);
		binder.forField(stradaDomiciliu).asRequired()
				.withValidator(str -> str.length() > 2, "Numele strazii sa fie mai mare decat 2 caractere")

				.bind(new ValueProvider<DeclaratieDeImpunereSalubrizareInfo, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public String apply(DeclaratieDeImpunereSalubrizareInfo source) {
						return null;
					}
				}, new Setter<DeclaratieDeImpunereSalubrizareInfo, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public void accept(DeclaratieDeImpunereSalubrizareInfo bean, String fieldvalue) {

					}
				});

		stradaDomiciliu.setErrorMessage("Strada Required !");
		infoPart8.add(stradaDomiciliu);
		infoLayout2.add(infoPart8);
		infoLayout2.setAlignItems(Alignment.CENTER);

		infoPart9 = new VerticalLayout();
		nrDomiciliu = new TextField("Nr. domiciliu:");
		nrDomiciliu.setRequiredIndicatorVisible(true);
		binder.forField(nrDomiciliu).asRequired()
				.bind(new ValueProvider<DeclaratieDeImpunereSalubrizareInfo, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public String apply(DeclaratieDeImpunereSalubrizareInfo source) {
						return null;
					}
				}, new Setter<DeclaratieDeImpunereSalubrizareInfo, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public void accept(DeclaratieDeImpunereSalubrizareInfo bean, String fieldvalue) {

					}
				});

		nrDomiciliu.setErrorMessage("Nr. Required !");
		infoPart9.add(nrDomiciliu);
		infoLayout2.add(infoPart9);
		infoLayout2.setAlignItems(Alignment.CENTER);

		infoPart10 = new VerticalLayout();
		BlDomiciliu = new TextField("Bl. domiciliu:");
		infoPart10.add(BlDomiciliu);
		infoLayout2.add(infoPart10);
		infoLayout2.setAlignItems(Alignment.CENTER);

		infoPart11 = new VerticalLayout();
		ScDomiciliu = new TextField("Sc. domiciliu:");
		infoPart11.add(ScDomiciliu);
		infoLayout2.add(infoPart11);
		infoLayout2.setAlignItems(Alignment.CENTER);

		infoPart12 = new VerticalLayout();
		ApDomiciliu = new TextField("Ap. domiciliu:");
		infoPart12.add(ApDomiciliu);
		infoLayout2.add(infoPart12);
		infoLayout2.setAlignItems(Alignment.CENTER);

		infoLayout3 = new HorizontalLayout();
		infoLayout3.setVisible(false);

		infoPart13 = new VerticalLayout();
		serieCIField = new TextField("Serie CI :");
		serieCIField.setRequiredIndicatorVisible(true);
		binder.forField(serieCIField).asRequired()
				.withValidator(str -> str.length() == 2, "Seria trebuie sa fie de 2 charactere")
				.bind(new ValueProvider<DeclaratieDeImpunereSalubrizareInfo, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public String apply(DeclaratieDeImpunereSalubrizareInfo source) {
						return null;
					}
				}, new Setter<DeclaratieDeImpunereSalubrizareInfo, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public void accept(DeclaratieDeImpunereSalubrizareInfo bean, String fieldvalue) {

					}
				});

		serieCIField.setErrorMessage("Serie Required!");
		infoPart13.add(serieCIField);
		infoLayout3.add(infoPart13);
		infoLayout3.setAlignItems(Alignment.CENTER);

		infoPart14 = new VerticalLayout();
		nrCIField = new TextField("Numar CI :");
		nrCIField.setRequiredIndicatorVisible(true);
		binder.forField(nrCIField).asRequired()
				.withValidator(str -> str.length() == 6, "Numar CI trebuie sa fie de 6 cifre")
				.withConverter(new StringToIntegerConverter("Must be Integer"))
				.bind(new ValueProvider<DeclaratieDeImpunereSalubrizareInfo, Integer>() {

					private static final long serialVersionUID = 1L;

					@Override
					public Integer apply(DeclaratieDeImpunereSalubrizareInfo source) {
						return null;
					}
				}, new Setter<DeclaratieDeImpunereSalubrizareInfo, Integer>() {

					private static final long serialVersionUID = 1L;

					@Override
					public void accept(DeclaratieDeImpunereSalubrizareInfo bean, Integer fieldvalue) {

					}
				});

		serieCIField.setErrorMessage("Numar CI Required!");
		infoPart14.add(nrCIField);
		infoLayout3.add(infoPart14);
		infoLayout3.setAlignItems(Alignment.CENTER);

		infoPart15 = new VerticalLayout();
		cnpField = new TextField("CNP:");
		cnpField.setRequiredIndicatorVisible(true);
		cnpField.addValueChangeListener(e -> {
			cnpField.setValue(cnpField.getValue().replaceAll("[a-zA-Z]+", ""));
		});

		binder.forField(cnpField).asRequired()
				.withValidator(str -> str.toString().length() == 13, "CNP-ul are 13 cifre")
				.withConverter(new StringToBigIntegerConverter("CNP-ul poate contine doar cifre"))

				.bind(new ValueProvider<DeclaratieDeImpunereSalubrizareInfo, BigInteger>() {

					private static final long serialVersionUID = 1L;

					@Override
					public BigInteger apply(DeclaratieDeImpunereSalubrizareInfo source) {
						return null;
					}
				}, new Setter<DeclaratieDeImpunereSalubrizareInfo, BigInteger>() {

					private static final long serialVersionUID = 1L;

					@Override
					public void accept(DeclaratieDeImpunereSalubrizareInfo bean, BigInteger fieldvalue) {

					}
				});

		cnpField.setErrorMessage("CNP Required !");
		infoPart15.add(cnpField);
		infoLayout3.add(infoPart15);
		infoLayout3.setAlignItems(Alignment.CENTER);

		infoPart16 = new VerticalLayout();
		LocDeMuncaField = new TextField("Loc De Munca:");
		LocDeMuncaField.setRequiredIndicatorVisible(true);
		binder.forField(LocDeMuncaField).asRequired()
				.withValidator(str -> str.length() > 4, "Locul de munca trebuie sa fie mai mare decat 4 charactere")
				.bind(new ValueProvider<DeclaratieDeImpunereSalubrizareInfo, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public String apply(DeclaratieDeImpunereSalubrizareInfo source) {
						return null;
					}
				}, new Setter<DeclaratieDeImpunereSalubrizareInfo, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public void accept(DeclaratieDeImpunereSalubrizareInfo bean, String fieldvalue) {

					}
				});

		LocDeMuncaField.setErrorMessage("Nume Required");
		LocDeMuncaField.setWidth("100%");
		LocDeMuncaField.setMaxLength(25);
		LocDeMuncaField.setMinWidth(20f, Unit.EM);
		infoPart16.add(LocDeMuncaField);
		infoLayout3.add(infoPart16);
		infoLayout3.setAlignItems(Alignment.CENTER);

		infoLayout4 = new HorizontalLayout();
		infoLayout4.setVisible(false);

		infoPart17 = new VerticalLayout();
		nrTelefon = new TextField("Nr. Telefon:");
		nrTelefon.setRequiredIndicatorVisible(true);

		binder.forField(nrTelefon).asRequired()
				.withValidator(str -> str.matches("^([00]{0,1}[\\d]{1,3}[0-9]{9,12})$"), "Numar de telefon invalid !")
				.withConverter(new StringToBigIntegerConverter("Nr. telefon poate contine doar cifre"))

				.bind(new ValueProvider<DeclaratieDeImpunereSalubrizareInfo, BigInteger>() {

					private static final long serialVersionUID = 1L;

					@Override
					public BigInteger apply(DeclaratieDeImpunereSalubrizareInfo source) {
						return null;
					}
				}, new Setter<DeclaratieDeImpunereSalubrizareInfo, BigInteger>() {

					private static final long serialVersionUID = 1L;

					@Override
					public void accept(DeclaratieDeImpunereSalubrizareInfo bean, BigInteger fieldvalue) {

					}
				});

		nrTelefon.setErrorMessage("Telefon Required !");
		infoPart17.add(nrTelefon);
		infoLayout4.add(infoPart17);
		infoLayout4.setAlignItems(Alignment.CENTER);

		infoPart18 = new VerticalLayout();
		emailField = new EmailField("E-mail: ");
		infoPart18.add(emailField);
		infoLayout4.add(infoPart18);
		infoLayout4.setAlignItems(Alignment.CENTER);

		infoLayout5 = new HorizontalLayout();
		infoLayout5.setVisible(false);

		infoPart19 = new VerticalLayout();
		locuitor = new TextField("Nume complet locuitor:");
		locuitor.setRequiredIndicatorVisible(true);
		locuitor.setWidth("100%");
		locuitor.setMaxLength(25);
		locuitor.setMinWidth(20f, Unit.EM);
		infoPart19.add(locuitor);
		infoLayout5.add(infoPart19);
		infoLayout5.setAlignItems(Alignment.CENTER);

		infoPart20 = new VerticalLayout();
		calitate = new ComboBox<String>("Calitate: ");
		calitate.setItems("Locatar Stabil", "Chirias", "Flotant");
		calitate.setPlaceholder("Selecteaza calitatea");
		calitate.setRequiredIndicatorVisible(true);
		infoPart20.add(calitate);
		infoLayout5.add(infoPart20);
		infoLayout5.setAlignItems(Alignment.CENTER);

		infoPart21 = new VerticalLayout();
		Observatii = new TextField("Observatii:");
		Observatii.setWidth("100%");
		Observatii.setMaxLength(25);
		Observatii.setMinWidth(20f, Unit.EM);
		infoPart21.add(Observatii);
		infoLayout5.add(infoPart21);
		infoLayout5.setAlignItems(Alignment.CENTER);

		infoPart22 = new VerticalLayout();
		Button addPers = new Button("add", VaadinIcon.PLUS.create());
		addPers.addClickListener(event -> {
			String persString = locuitor.getValue().trim() + "|" + calitate.getOptionalValue().get() + "|"
					+ Observatii.getValue().trim();
			pers.put(persSize++, persString);
			Notification.show("Person added!", 3000, Position.BOTTOM_END);
			persList.add(new Person(locuitor.getValue().trim(), calitate.getOptionalValue().get(),
					Observatii.getValue().trim()));
			grid.setItems(persList);
			grid.getDataProvider().refreshAll();
			grid.recalculateColumnWidths();
		});
		infoPart22.add(addPers);
		infoPart22.setAlignItems(Alignment.BASELINE);
		infoLayout5.add(infoPart22);
		infoLayout5.setAlignItems(Alignment.CENTER);

		infoLayout6 = new HorizontalLayout();
		infoLayout6.setVisible(false);

		infoPart23 = new VerticalLayout();
		grid.setAllRowsVisible(true);
		grid.addColumn(Person::nume).setHeader("Name");
		grid.addColumn(Person::calitate).setHeader("Calitate");
		grid.addColumn(Person::observatii).setHeader("Observatii");
		grid.addColumn(new ComponentRenderer<>(Button::new, (button, person) -> {
			button.addThemeVariants(ButtonVariant.LUMO_ICON, ButtonVariant.LUMO_ERROR, ButtonVariant.LUMO_TERTIARY);
			button.addClickListener(e -> this.removeInvitation(person));
			button.setIcon(new Icon(VaadinIcon.TRASH));
		})).setHeader("Elimina");

		infoPart23.add(grid);
		infoLayout6.add(infoPart23);
		infoLayout6.setAlignItems(Alignment.CENTER);

		content.add(infoLayout);
		content.setAlignItems(Alignment.CENTER);
		content.add(infoLayout2);
		content.setAlignItems(Alignment.CENTER);
		content.add(infoLayout3);
		content.setAlignItems(Alignment.CENTER);
		content.add(infoLayout4);
		content.setAlignItems(Alignment.CENTER);
		content.add(infoLayout5);
		content.setAlignItems(Alignment.CENTER);
		content.add(infoLayout6);
		content.setAlignItems(Alignment.CENTER);

		generateLayout = new HorizontalLayout();
		generate = new Button("Generate", VaadinIcon.FILE_PROCESS.create());
		generate.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
		generate.getClassNames().add("frendly");
		generate.addClickListener(evt -> {
			if (complete.getValue()) {

				map.put("nume", PDFHelper.capitalizeWords(numeField.getValue()));
				map.put("localitateLocuinta", localitateLocuinta.getValue().trim());
				map.put("strLocuinta", stradaLocuinta.getValue().trim());
				map.put("nrLocuinta", nrLocuinta.getValue().trim());
				map.put("blLocuinta", BlLocuinta.getValue().trim());
				map.put("scLocuinta", ScLocuinta.getValue().trim());
				map.put("apLocuinta", ApLocuinta.getValue().trim());
				map.put("localitateDomiciliu", localitateDomiciliu.getValue().trim());
				map.put("stradaDomiciliu", stradaDomiciliu.getValue().trim());
				map.put("nrDomiciliu", nrDomiciliu.getValue().trim());
				map.put("blDomiciliu", BlDomiciliu.getValue().trim());
				map.put("scDomiciliu", ScDomiciliu.getValue().trim());
				map.put("apDomiciliu", ApDomiciliu.getValue().trim());
				map.put("serieCI", serieCIField.getValue().trim());
				map.put("nrCI", nrCIField.getValue().trim());
				map.put("cnp", cnpField.getValue().trim());
				map.put("locDeMunca", LocDeMuncaField.getValue().trim());
				map.put("telefon", nrTelefon.getValue().trim());
				map.put("email", emailField.getValue().trim());

			}
			numeField.clear();
			localitateLocuinta.clear();
			stradaLocuinta.clear();
			nrLocuinta.clear();
			BlLocuinta.clear();
			ScLocuinta.clear();
			ApLocuinta.clear();
			localitateDomiciliu.clear();
			stradaDomiciliu.clear();
			nrDomiciliu.clear();
			BlDomiciliu.clear();
			ScDomiciliu.clear();
			ApDomiciliu.clear();
			serieCIField.clear();
			nrCIField.clear();
			cnpField.clear();
			LocDeMuncaField.clear();
			nrTelefon.clear();
			emailField.clear();

			DeclaratieDeImpunereSalubrizareCreator pdfcr = new DeclaratieDeImpunereSalubrizareCreator(map,
					Utils.getTimeStr());
			String fn = pdfcr.getID();

			RouteConfiguration.forSessionScope().removeRoute(DeclaratieDeImpunereSalubrizarePDF.class);
			RouteConfiguration.forSessionScope().setRoute(DeclaratieDeImpunereSalubrizarePDF.NAME,
					DeclaratieDeImpunereSalubrizarePDF.class);
			Map<String, String> sss = new HashMap<String, String>();
			sss.put("tm", fn);

			UI.getCurrent().navigate("DeclaratieDeImpunereSalubrizarePDF", QueryParameters.simple(sss));

		});
		generateLayout.add(generate);
		binder.addStatusChangeListener(event -> {
			if (!complete.getValue() || binder.isValid()) {
				generate.setEnabled(true);
			} else {
				generate.setEnabled(false);
			}
		});
		content.add(generateLayout);
		content.setAlignItems(Alignment.CENTER);
		content.setMargin(false);
		add(content);

	}

	private void toggleVisibility() {
		infoLayout.setVisible(!infoLayout.isVisible());
		infoLayout2.setVisible(!infoLayout1.isVisible());
		infoLayout2.setVisible(!infoLayout2.isVisible());
		infoLayout3.setVisible(!infoLayout3.isVisible());
		infoLayout4.setVisible(!infoLayout4.isVisible());
		infoLayout5.setVisible(!infoLayout5.isVisible());
		infoLayout6.setVisible(!infoLayout6.isVisible());

		generate.setEnabled(!generate.isEnabled());
	}

	@Override
	public void afterNavigation(AfterNavigationEvent event) {

		RouteConfiguration.forSessionScope().removeRoute(NAME);
	}

	private void removeInvitation(Person person) {
		if (person == null)
			return;
		persList.remove(person);
		grid.getDataProvider().refreshAll();
	}

}

record Person(String nume, String calitate, String observatii) {
};
